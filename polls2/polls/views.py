from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render, get_object_or_404
from django.views import generic
from .models import Question, Choice
from django.utils import timezone

# Create your views here.
class IndexView(generic.ListView):
	def get_queryset(self):
		return Question.objects.filter(pub_date__lte=timezone.now()).order_by('-pub_date')[:5]
	template_name = 'polls/index.html'
	context_object_name = 'latest_question_list'

class DetailView(generic.DetailView):
	model = Question
	template_name = 'polls/detail.html'
	def get_queryset(self):
		return Question.objects.filter(pub_date__lte=timezone.now())

class ResultsView(generic.DetailView):
	model = Question
	template_name = 'polls/results.html'

def vote(request, question_id):
	question = get_object_or_404(Question, question_id)
	try:
		selected_choice = request.POST['choice']
	except(KeyError, Choice.DoesNotExist):
		context = { 'question':question_id, 'error_message':'No Choice Selected' }
		return render(request, 'polls/detail.html', context)
	else:
		selected_choice = F('votes')+1
		selected_choice.save()
		return HttpResponseRedirect(reverse('polls/detail'), args=(question_id,))