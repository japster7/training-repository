from django.test import TestCase
from .models import Choice,Question
from django.utils import timezone
from django.urls import reverse
import datetime

# Create your tests here.
class QuestionModelTests(TestCase):
	def test_with_published_recently_with_future_question(self):
		time = timezone.now() + datetime.timedelta(days=1)
		q = Question(question_text = 'Future question.', pub_date=time)
		return self.assertIs(q.was_published_recently(), False)
	def test_with_published_recently_with_past_question(self):
		time = timezone.now() - datetime.timedelta(days=1,hours=1)
		q = Question(question_text='Past question.', pub_date=time)
		return self.assertIs(q.was_published_recently(), False)
	def test_with_published_recently_with_present_question(self):
		time = timezone.now() - datetime.timedelta(hours=12)
		q = Question(question_text='Recent question.', pub_date=time)
		return self.assertIs(q.was_published_recently(), True)

def create_question(question_text, days):
	time = time = timezone.now() + datetime.timedelta(days=days)
	return Question.objects.create(question_text=question_text,pub_date=time)

class QuestionIndexViewTests(TestCase):
	def test_with_no_questions(self):
		response = self.client.get(reverse('polls:index'))
		self.assertEquals(response.status_code,200)
		self.assertContains(response,'No poll questions yet')
		self.assertQuerysetEqual(response.context['latest_question_list'],[])

	def test_with_future_question(self):
		create_question('Future question.', 5)
		response = self.client.get(reverse('polls:index'))
		self.assertEquals(response.status_code,200)
		self.assertQuerysetEqual(response.context['latest_question_list'],[])

	def test_with_past_question(self):
		create_question('Past question.', -5)
		response = self.client.get(reverse('polls:index'))
		self.assertEquals(response.status_code,200)
		self.assertQuerysetEqual(response.context['latest_question_list'],['<Question: Past question.>'])

	def test_with_past_and_future_questions(self):
		create_question('Past question.', -5)
		create_question('Future question.', 5)
		response = self.client.get(reverse('polls:index'))
		self.assertEquals(response.status_code,200)
		self.assertQuerysetEqual(response.context['latest_question_list'],['<Question: Past question.>'])

	def test_with_two_past_questions(self):
		create_question('Past question 1.', -5)
		create_question('Past question 2.', -1)
		response = self.client.get(reverse('polls:index'))
		self.assertEquals(response.status_code,200)
		self.assertQuerysetEqual(response.context['latest_question_list'],['<Question: Past question 2.>','<Question: Past question 1.>'])

class QuestionDetailViewTests(TestCase):
	def test_with_future_question(self):
		q = create_question('Future question.',10)
		url = reverse('polls:detail', args=(q.id,))
		response = self.client.get(url)
		self.assertEquals(response.status_code, 404)

	def test_with_past_question(self):
		q = create_question('Past question.',-10)
		url = reverse('polls:detail', args=(q.id,))
		response = self.client.get(url)
		self.assertContains(response, q.question_text)