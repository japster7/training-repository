from django.contrib import admin
from .models import Choice,Question

# Register your models here.
class ChoiceInline(admin.TabularInline):
	model = Choice
	extra = 1

class QuestionAdmin(admin.ModelAdmin):
	list_display = ('question_text','pub_date','was_published_recently')
	fieldsets = [
		('Basic', {'fields':['question_text']}),
		('Date Input',{'fields':['pub_date'], 'classes':['collapse','show']}),
	]
	inlines = [ChoiceInline]

admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice)