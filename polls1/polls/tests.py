from django.test import TestCase
from django.utils import timezone
from django.urls import reverse
import datetime

from .models import Question
# Create your tests here.

class QuestionModelTests(TestCase):

	def test_was_published_recently_with_future_question(self):
		time = timezone.now() + datetime.timedelta(days=1)
		future_question = Question(pub_date=time)
		self.assertIs(future_question.was_published_recently(), False)

	def test_was_published_recently_with_old_question(self):
		time = timezone.now() - datetime.timedelta(days=1, seconds=1)
		future_question = Question(pub_date=time)
		self.assertIs(future_question.was_published_recently(), False)

	def test_was_published_recently_with_recent_question(self):
		time = timezone.now() - datetime.timedelta(hours=23,minutes=59,seconds=59)
		future_question = Question(pub_date=time)
		self.assertIs(future_question.was_published_recently(), True)


def create_question(question_text, days):
	time = timezone.now() + datetime.timedelta(days=days)
	return Question.objects.create(question_text=question_text, pub_date=time)


class QuestionIndexViewTests(TestCase):

	def test_no_questions(self):
		response = self.client.get(reverse('polls:index'))
		self.assertIs(response.status_code, 200)
		self.assertContains(response, 'No polls are available')
		self.assertQuerysetEqual(response.context['latest_question_list'], [])

	def test_past_question(self):
		create_question('Past question.', -30)
		response = self.client.get(reverse('polls:index'))
		self.assertIs(response.status_code, 200)
		self.assertQuerysetEqual(response.context['latest_question_list'], ['<Question: Past question.>'])

	def test_future_question(self):
		create_question('Future question.', 30)
		response = self.client.get(reverse('polls:index'))
		self.assertIs(response.status_code, 200)
		self.assertQuerysetEqual(response.context['latest_question_list'], [])

	def test_past_and_future_question(self):
		create_question('Past question.', -30)
		create_question('Future question.', 30)
		response = self.client.get(reverse('polls:index'))
		self.assertIs(response.status_code, 200)
		self.assertQuerysetEqual(response.context['latest_question_list'], ['<Question: Past question.>'])

	def test_two_past_questions(self):
		create_question('Past question 1.', -30)
		create_question('Past question 2.', -5)
		response = self.client.get(reverse('polls:index'))
		self.assertIs(response.status_code, 200)
		self.assertQuerysetEqual(response.context['latest_question_list'], ['<Question: Past question 2.>','<Question: Past question 1.>'])


class QuestionDetialViewTests(TestCase):

	def test_future_question(self):
		q = create_question('Future question.', 5)
		url = reverse('polls:detail', args=(q.id,))
		response = self.client.get(url)
		self.assertEqual(response.status_code, 404)

	def test_past_question(self):
		q = create_question('Future question.', -5)
		url = reverse('polls:detail', args=(q.id,))
		response = self.client.get(url)
		self.assertContains(response, q.question_text)